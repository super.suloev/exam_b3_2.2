from flask import Flask, request, render_template

app = Flask(__name__)
output_string = None


@app.route('/', methods=['GET', 'POST'])
def string_magic():
    global output_string

    if request.method == "POST":
        str_1 = request.form.get('string1')
        str_2 = request.form.get('string2')
        output_string = str_1[::-1] + str_2[::-1]

    return render_template("index.html", output_string=output_string)


if __name__ == '__main__':
    app.run()
